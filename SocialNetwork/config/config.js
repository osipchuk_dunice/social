var path = require('path'),
    rootPath = path.normalize(__dirname + '/..'),
    env = process.env.NODE_ENV || 'development';

var config = {
  development: {
    root: rootPath,
    app: {
      name: 'task3'
    },
    port: 3000,
    db: 'mongodb://localhost/task3-development'

  },

  test: {
    root: rootPath,
    app: {
      name: 'task3'
    },
    port: 3000,
    db: 'mongodb://localhost/task3-test'

  },

  production: {
    root: rootPath,
    app: {
      name: 'task3'
    },
    port: 3000,
    db: 'mongodb://localhost/task3-production'

  }
};

module.exports = config[env];

var mongoose = require('mongoose');
var ObjectId = mongoose.Schema.Types.ObjectId;

var DialogsSchema = new mongoose.Schema({
  messages: [{ type: ObjectId, ref: 'Messages' }]

});

var Dialog = mongoose.model('Dialogs', DialogsSchema);

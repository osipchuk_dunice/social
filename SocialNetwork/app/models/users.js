var mongoose = require('mongoose');
var ObjectId = mongoose.Schema.Types.ObjectId;

var UsersSchema = new mongoose.Schema({
  name: {type: String},
  lastname: {type: String},
  username: {type: String},
  email: {type: String},
  password: {type: String},
  date: {type: String},
  city: {type: String},
  friends: [{ type: ObjectId, ref: 'Friends' }],
  avatar: {type: String}

});

UsersSchema.methods.validPassword = function( pwd ) {
  return ( this.password === pwd );
};

var User = mongoose.model('Users', UsersSchema);

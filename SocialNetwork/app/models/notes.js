var mongoose = require('mongoose');

var toDoSchema = new mongoose.Schema({
    name: {type: String, default: "greet everyone here"},
    done: {type: Boolean, default: false},
    user: {type: String, default: null}
} );

var Notes = mongoose.model('Notes', toDoSchema);

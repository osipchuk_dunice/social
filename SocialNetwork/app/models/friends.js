/**
 * Created by dunice on 2/24/15.
 */
var mongoose = require('mongoose');
var ObjectId = mongoose.Schema.Types.ObjectId;

var friendSchema = new mongoose.Schema({
  fr1:[{ type: ObjectId, ref: 'Users' }],
  fr2:[{ type: ObjectId, ref: 'Users' }]
});


var Friends = mongoose.model('Friends', friendSchema);

exports.Friends = mongoose.model('Friends', friendSchema);


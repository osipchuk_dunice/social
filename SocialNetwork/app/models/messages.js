var mongoose = require('mongoose');
var ObjectId = mongoose.Schema.Types.ObjectId;

var messageSchema = new mongoose.Schema({
  sender: [{ type: ObjectId, ref: 'Users' }],
  receiver:[{ type: ObjectId, ref: 'Users' }],
  name: { type: String, default:null},
  date:{ type: Date, default: Date.now }
});

var Messages = mongoose.model('Messages', messageSchema);

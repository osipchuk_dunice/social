/**
 * Created by dunice on 2/24/15.
 */
var mongoose = require('mongoose');
var ObjectId = mongoose.Schema.Types.ObjectId;

var bidSchema = new mongoose.Schema({
  sender_id: [{ type: ObjectId, ref: 'Users' }],
  receiver_id:[{ type: ObjectId, ref: 'Users' }]

});


var Bids = mongoose.model('Bids', bidSchema);

exports.Bids = mongoose.model('Bids', bidSchema);

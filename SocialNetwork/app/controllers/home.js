var express = require('express'),
    router = express.Router(),
    mongoose = require('mongoose'),
    Notes = mongoose.model('Notes');
    User = mongoose.model('Users');
    Bids = mongoose.model('Bids');
    Messages = mongoose.model('Messages');
    Friends = mongoose.model('Friends');
    passport = require('passport');
    LocalStrategy = require('passport-local').Strategy;
    fs = require('fs');
    http = require('http');






module.exports = function (app) {
  app.use('/', router);
};


router.get('/', function (req, res, next) {

  Notes.find(function (err, notes) {
    if (err) return next(err);
    res.render('index', {
      notes:notes,
      user: req.user
    });
  });
});


router.get('/getPersonal', function(req, res){
  var userID = req.user._id;
  User.findOne({ _id: userID  }, function(err, user) {
    if(err) console.log(err);
    res.send(user);
  })

});

router.post('/save', function(req, res) {
  var userData = {
    username: req.body.username,
    password: req.body.password,
    email: req.body.email,
    name: req.body.name,
    lastname:req.body.lastname,
    date: req.body.date,
    city: req.body.city
  };

  var userID = req.user._id;
  User.update({ _id: userID }, userData, function (err) {
    if(err) console.log(err);

  });
});



var multer  = require('multer');
var done=false;

router.use(multer({ dest: './public/uploads/',
  rename: function (fieldname, filename) {
    return filename+Date.now();
  },
  onFileUploadStart: function (file) {
    console.log(file.originalname + ' is starting ...')
  },
  onFileUploadComplete: function (file) {
    console.log(file.fieldname + ' uploaded to  ' + file.path)
    done=true;
  }
}));

router.get('/#/profile/',function(req,res){
  res.sendfile('/#/profile/' + req.user._id);
});

router.post('/api/photo',function(req,res){
  if(done==true) {
    console.log(req.files.userPhoto.name);

    var userData = {
      avatar: req.files.userPhoto.name
    };
    var userID = req.user._id;
    User.update({_id: userID}, userData, function (err) {
      if (err) console.log(err);
      res.redirect('/#/profile/' + req.user._id);
    });
  }
});




//router.post('/saveimg', function(req, res){
//  console.log(req);
//
//  //var userData = {
//  //  avatar: req.body.avatar
//  //};
//  //
//  //var userID = req.user._id;
//  //User.update({ _id: userID }, userData, function (err) {
//  //  if (err) console.log(err);
//    res.redirect('/#/profile/' + req.user._id);
//  //});
//
//});





router.post('/search', function(req, res) {

  User.findOne({'name':'Bill'}, function (err, user) {
    if (err) console.log(err);
    console.log(user.name, user.lastname);
    res.redirect('/#/search');
  });

});



router.get('/user/:id',function(req, res){
  User.find({_id: req.param('id')}).exec(function(err, user){
    //console.log('--------->>>>>> ', arguments);
    res.send(user);
  })
});

router.get('/getUsers', function(req, res){
  User.find( function (err, users) {
    if(err) console.log(err);
    res.send(users);
  })
});

router.post('/addFriend', function(req, res){
  console.log(req.body);
  var frField = req.body.id[0].id;
  var obj = {fr1: frField, fr2: req.user._id};
  Friends.create( obj, function(err, data) {
    if(!err) console.log(data);
    res.send(data);
  });

});

router.get('/getFriends', function(req, res){
  Friends.find({$or: [{fr1:req.user._id}, {fr2: req.user._id}]}, function (err, friends) {
    if(err) console.log(err);
    res.send(friends);
  })
});




router.post('/addBid', function(req, res) {
  console.log(req.body.profile);
  var bidData = {
    sender_id: req.user._id,
    receiver_id: req.body.profile.data[0]._id
  };

  var bid = new Bids(bidData);
  bid.save( function (err, data) {
    if(err) console.log(err);
    res.send(data);
  });
});



router.get('/getBids', function(req, res){
 Bids.find({receiver_id: req.user._id})
    .populate('sender_id')
    .exec(function(err, bids) {
      res.send(bids);
     console.log(bids);
    })

});


router.delete('/del/:id', function(req, res){
  var id=req.params.id;
  Bids.remove({_id: id}, function(err, data) {
    res.send('OK');
  })

});




router.get('/getM', function(req, res){
  Messages.find({receiver:req.user.id})
    .populate('sender')
    .exec(function(err, messages) {
      res.send(messages);
      console.log(messages);
    })

});


router.post('/addM', function(req, res) {
  console.log(req.body.profile);
  var newName = req.body.newName;
  var userField = req.user ? req.user._id : null;
  var receiverField = req.body.profile.data[0]._id;
  var obj = {name: newName, sender: userField, receiver: receiverField};
  Messages.create( obj, function(err, data) {
    if(!err) console.log(data);
    res.send(data);

  });

});


router.get('/getD/:id', function(req, res){
  console.log(req.params.id);
  Messages.find( {$or: [{sender:req.user._id, receiver:req.params.id}, {sender: req.params.id, receiver: req.user._id}]})
    .populate('sender')
    .exec(function(err, messages) {
      res.send(messages);
      console.log(messages);
    })

});

//PASSPORT//////////////////////////////////////////////////////////////////////////




var passport = require('passport');
var LocalStrategy = require('passport-local').Strategy;


router.get('/logout', function(req, res){
  req.logout();
  res.redirect('/');
});


router.post('/login',
  passport.authenticate('local', {
    failureRedirect: '/#/'
  }), function(req, res) {
    res.redirect('/#/profile/' + req.user._id);
  }
);

router.post('/register', function(req, res, next) {
  var userData = {
    username: req.body.username,
    password: req.body.password,
    email: req.body.email,
    name: req.body.name,
    lastname:req.body.lastname,
    date: req.body.date
  };

  var user = new User(userData);
  user.save(function (err) {
    return err ? next(err) : req.logIn(user, function (err) {
      return err ? next(err) : res.redirect('/#/profile/' + req.user._id);
    });
  });
});




router.get('/login', function(req, res, next) {
  passport.authenticate('local', function(err, user, info) {
    console.log(err);
    console.log(user);
    console.log(info);
    if (err) { return next(err); }
    if (!user) { return res.redirect('/login'); }
    req.logIn(user, function(err) {
      if (err) { return next(err); }
      return res.redirect('/users/' + user.username);
    });
  })(req, res, next);
});


passport.use(new LocalStrategy(
  function(username, password, done) {
    User.findOne({ username: username }, function (err, user) {
      if (err) { return done(err); }
      if (!user) {
        return done(null, false, { message: 'Incorrect username.' });
      }
      if (!user.validPassword(password)) {
        return done(null, false, { message: 'Incorrect password.' });
      }
      return done(null, user);
    });
  }
));

passport.serializeUser(function(user, done) {
  done(null, user.id);
});

passport.deserializeUser(function(id, done) {
  User.findById(id, function(err, user) {
    done(err, user);
  });

});


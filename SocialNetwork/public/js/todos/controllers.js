var SocialControllers = angular.module('SocialControllers',[]);

SocialControllers.run(['$rootScope', '$http', function($rootScope, $http) {


}]);



SocialControllers.controller('AllCtrl', ['$scope', '$rootScope', '$http', function($scope, $rootScope, $http) {

  $scope.getUsers = function() {
    $http.get('/getUsers').
      success(function(data) {
        $scope.users=data;
      }).
      error(function (data) {
        console.log(data)
      })

  };

  $scope.getUsers();





}]);



SocialControllers.controller('SearchCtrl', ['$scope', '$rootScope', '$http', function($scope, $rootScope, $http) {

  $scope.getUsers = function() {
    $http.get('/getUsers').
      success(function(data) {
        $scope.users=data;
      }).
      error(function (data) {
        console.log(data)
      })

  };

  $scope.getUsers();




}]);



SocialControllers.controller('PersonalCtrl', ['$scope', '$rootScope', '$http', 'profile', function($scope, $rootScope, $http, profile) {
  console.log('=====>>>> ', profile);
  $scope.user = {};
  $scope.getPersonal = function() {
    $http.get('/getPersonal').success(function(data){
      $scope.user = data;
      $scope.profile = profile;
      console.log($scope.profile);

    }).error(function (data) {
      console.log(data)
    })

  };
  $scope.getPersonal();

  $scope.addBid = function(e) {
    console.log($scope.profile);
    var d = {profile: $scope.profile};
   $http.post('/addBid', d).
     success(function(data){
       $scope.bid = data;

     }).
     error(function(data){
       console.log(data);
     });
  };

  $scope.addFriend = function(bid) {
    console.log(bid);
    $http.post('/addFriend', {id : bid.sender_id}).
      success(function(data){
        $scope.friends.push(data);
        $scope.bid = bid;
        $scope.asd = true;
      }).
      error(function(data){
        console.log(data);
      });
  };

  $scope.getFriends = function() {
    $http.get('/getFriends').
      success(function(data) {
        $scope.friends=data;
      }).
      error(function (data) {
        console.log(data)
      })

  };

  $scope.getFriends();

  $scope.getUsers = function() {
    $http.get('/getUsers').
      success(function(data) {
        $scope.users=data;
      }).
      error(function (data) {
        console.log(data)
      })

  };

  $scope.getUsers();

  $scope.getBids = function() {
    $http.get('/getBids').
      success(function(data) {
        $scope.bids=data;
      }).
      error(function (data) {
        console.log(data)
      })

  };

  $scope.getBids();


  $scope.addMessage = function(e){
    if (e.keyCode != 13 || !$scope.newMessage.length) return;

    console.log($scope.profile);
    var d = {profile: $scope.profile};

    $http.post('/addM',{newName: $scope.newMessage, profile: $scope.profile}).
      success(function(data){
        $scope.messages.push(data);

      }).
      error(function(data){
        console.log(data);
      });
    $scope.newMessage = '';

  };


  $rootScope.getM = function() {
    $http.get("/getM").
      success(function (data) {
        $rootScope.messages = data;
      }).
      error(function (data) {
        console.log(data)
      });
  };

  $scope.getM();



  $scope.getDialog = function(e) {
    console.log($scope.profile);
    $http.get('/getD/'+ $scope.profile.data[0]._id).
      success(function (data) {
        $scope.messages = data;
        $scope.abc = true;
      }).
      error(function (data) {
        console.log(data)
      });
  };


  $scope.removeBid = function(bid) {
    $http.delete('/del/' + bid._id).success(function(data){
      if(data != 'OK') return;
      for(var i = 0; i<=$rootScope.bids.length; i++){
        if($rootScope.bids[i]._id == bid._id){
          $rootScope.bids.splice(i,1);
          break;
        }
      }
    }).error(function(data){
      console.log(data);
    })
  };





  $scope.addAvatar = function(e){
    $http.post('/addA')

  };



}]);



SocialControllers.controller('validateCtrl', ['$scope', '$rootScope', '$http', function($scope, $rootScope, $http) {
  $scope.email = '';
}]);



SocialControllers.controller('EditPersonalCtrl', ['$scope', '$rootScope', '$http', function($scope, $rootScope, $http) {
  $scope.user = {};
  $scope.getPersonal = function() {
    $http.get('/getPersonal').success(function(data){
      $scope.user = data;
    }).error(function (data) {
      console.log(data)
    })

  };
  $scope.getPersonal();

}]);

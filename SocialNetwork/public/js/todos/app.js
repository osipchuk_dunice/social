var SocialApp = angular.module('SocialApp', ['ngRoute', 'ngResource', 'SocialDirectives', 'SocialControllers']);

SocialApp.config(['$routeProvider',
    function($routeProvider) {
        $routeProvider.
            when('/', {
                templateUrl: 'partials/all.html',
                controller: 'AllCtrl'}).
            when('/profile/:id', {
              templateUrl: 'partials/profile.html',
              controller: 'PersonalCtrl',
              resolve:{
                profile:['$http', '$route', function($http, $route){
                  return $http.get('/user/' + $route.current.params.id).success(function(data){
                    return data;
                  }).error(function (data) {
                    console.log(data)
                  })
                }]
              }
            }).
            when('/editprofile', {
              templateUrl: 'partials/editprofile.html',
              controller: 'EditPersonalCtrl'}).
            when('/search', {
              templateUrl: 'partials/search.html',
              controller: 'SearchCtrl'}).
            otherwise({
                templateUrl: 'partials/all.html',
                controller: 'AllCtrl'
            });
    }]);



function validateForm()
{
  var x=document.forms["myForm"]["email"].value;
  if (x==null || x=="")
  {
    alert("Необходимо заполнить поля username|password|email!");
    return false;
  }
}
